﻿using System.IdentityModel.Tokens.Jwt;
using System.Linq;

namespace ActivityRecords.Gateway.Api.Service.Services
{
    public class JwtObtainer
    {
        public readonly string Token;
        public JwtObtainer(string token)
        {
            Token = token;
        }

        public int GetId()
        {
            var token = new JwtSecurityTokenHandler().ReadJwtToken(Token);
            return int.Parse(token.Claims.First(claim => claim.Type == "id").Value);
        }
    }
}
