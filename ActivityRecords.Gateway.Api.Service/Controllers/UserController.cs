﻿using ActivityRecords.Gateway.Api.Service.Services;
using ActivityRecords.Data.Api.Model.Interfaces;
using ActivityRecords.Data.Api.Model.User;
using ActivityRecords.Identity.Api.Model.Entities;
using ActivityRecords.Identity.Api.Model.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ActivityRecords.Gateway.Api.Service.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class UserController : Controller
    {
        private IUserInfoClient _userInfoClient;
        private IIdentityClient _identityClient;
        private JwtObtainer _jwtObtainer;
        public UserController(IUserInfoClient userClient, IIdentityClient registrationClient, IHttpContextAccessor httpContextAccessor)
        {
            _userInfoClient = userClient;
            _identityClient = registrationClient;

            var request = httpContextAccessor.HttpContext.Request;
            _jwtObtainer = new JwtObtainer(request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last());
        }

        [Authorize]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            try
            {
                var userId = _jwtObtainer.GetId();
                if (userId != id)
                {
                    return new JsonResult(new { error = "ids don't match" }) { StatusCode = StatusCodes.Status403Forbidden };
                }
                return new JsonResult(await _userInfoClient.GetByIdAsync(userId));
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
        }

        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAsync(int id, UpdateUserModel updateUser)
        {
            try
            {
                var userId = _jwtObtainer.GetId();
                if (id != userId)
                {
                    return new JsonResult(new { error = "ids don't match" }) { StatusCode = StatusCodes.Status403Forbidden };
                }
                if (!string.IsNullOrEmpty(updateUser.Login) || !string.IsNullOrEmpty(updateUser.Password))
                {
                    Credentials credentials = new Credentials { Login = updateUser.Login, Password = updateUser.Password };
                    await _identityClient.UpdateAsync(userId, credentials);
                }
                await _userInfoClient.UpdateAsync(userId, updateUser);
                return Ok();
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        [Route("")]
        public async Task<IActionResult> CreateAsync(CreationUserModel creationUser)
        {
            try
            {
                Credentials credentials = new Credentials { Login = creationUser.Login, Password = creationUser.Password };
                int id = await _identityClient.RegisterAsync(credentials);
                creationUser.Id = id;
                await _userInfoClient.CreateAsync(creationUser);
                return Ok();
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
        }

        [Authorize]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            try
            {
                var userId = _jwtObtainer.GetId();
                if (userId != id)
                {
                    return new JsonResult(new { error = "ids don't match" }) { StatusCode = StatusCodes.Status403Forbidden };
                }
                await _userInfoClient.DeleteAsync(userId);
                await _identityClient.DeleteAsync(userId);
                return Ok();
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
