﻿using ActivityRecords.Data.Api.Model.ActivityType;
using ActivityRecords.Data.Api.Model.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Threading.Tasks;

namespace ActivityRecords.Gateway.Api.Service.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class ActivityTypeController : Controller
    {
        private IActivityTypeClient _activityTypeClient;
        public ActivityTypeController(IActivityTypeClient activityTypeClient)
        {
            _activityTypeClient = activityTypeClient;
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreationActivityTypeModel creationActivityType)
        {
            try
            {
                return new OkObjectResult(await _activityTypeClient.Create(creationActivityType));
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            try
            {
                return new OkObjectResult(await _activityTypeClient.GetById(id));
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet("user/{userId}")]
        public async Task<IActionResult> GetActivityTypesByUserId(int userId)
        {
            try
            {
                return new OkObjectResult(await _activityTypeClient.GetByUserId(userId));
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteById(int id)
        {
            try
            {
                await _activityTypeClient.Delete(id);
                return Ok();
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, UpdateActivityTypeModel updateActivity)
        {
            try
            {
                return new OkObjectResult(await _activityTypeClient.Update(id, updateActivity));
            }
            catch (HttpRequestException e)
            {
                return BadRequest(e.Message);
            }
        }

    }
}
