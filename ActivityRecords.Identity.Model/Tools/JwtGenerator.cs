﻿using ActivityRecords.Identity.Entities;
using ActivityRecords.Identity.Model.Entities;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace ActivityRecords.Identity.Model.Tools
{
    public static class JwtGenerator
    {
        public static string Generate(User user, IOptions<AuthOptions> authOptions)
        {
            var authParams = authOptions.Value;

            var securityKey = authOptions.Value.GetSymmetricSecurityKey();
            var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new List<Claim>()
            {
                new Claim("login", user.Login),
                new Claim("id", user.Id.ToString())
            };
            var token = new JwtSecurityToken(
                issuer: authParams.Issuer,
                audience: authParams.Audience,
                claims: claims,
                expires: DateTime.UtcNow.AddSeconds(authParams.TokenLifetime),
                signingCredentials: signingCredentials);
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
