﻿using System;

namespace ActivityRecords.Identity.Model.Exceptions
{
    public class RepositoryException : Exception
    {
        public RepositoryException(string message) : base(message)
        {

        }
    }
}
