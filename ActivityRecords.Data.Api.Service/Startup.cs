using ActivityRecords.Data.Repository;
using ActivityRecords.Data.Repository.Model.Interfaces;
using ActivityRecords.Data.Repository.Repositories;
using ControllerMapperProfiles = ActivityRecords.Data.Api.Service.MapperProfiles;
using RepositoryMapperProfiles = ActivityRecords.Data.Repository.MapperProfiles;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace ActivityRecords.Data.Api.Service
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddDbContext<DataContext>(
                options => options.UseSqlServer(Configuration.GetConnectionString("Default")),
                ServiceLifetime.Scoped);

            services.AddSwaggerGen();

            services.AddAutoMapper(mc =>
            {
                mc.AddProfile(typeof(ControllerMapperProfiles.UserProfile));
                mc.AddProfile(typeof(ControllerMapperProfiles.ActivityProfile));
                mc.AddProfile(typeof(ControllerMapperProfiles.ActivityTypeProfile));
                mc.AddProfile(typeof(RepositoryMapperProfiles.UserProfile));
                mc.AddProfile(typeof(RepositoryMapperProfiles.ActivityProfile));
                mc.AddProfile(typeof(RepositoryMapperProfiles.ActivityDetailProfile));
                mc.AddProfile(typeof(RepositoryMapperProfiles.ActivityTypeProfile));
                mc.AddProfile(typeof(RepositoryMapperProfiles.ActivityTypeDetailProfile));
            });

            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IActivityTypeRepository, ActivityTypeRepository>();
            services.AddScoped<IActivityRepository, ActivityRepository>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(cfg => cfg.SwaggerEndpoint("/swagger/v1/swagger.json", "Activity Records v1"));

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
