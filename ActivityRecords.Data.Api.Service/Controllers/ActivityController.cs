﻿using ActivityRecords.Data.Api.Model.Activity;
using ActivityRecords.Data.Repository.Model.Entities;
using ActivityRecords.Data.Repository.Model.Exceptions;
using ActivityRecords.Data.Repository.Model.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace ActivityRecords.Data.Api.Service.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ActivityController : Controller
    {
        private IActivityRepository _activityRepository;
        private IMapper _mapper;

        public ActivityController(IActivityRepository activityRepository, IMapper mapper)
        {
            _activityRepository = activityRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Creates an <see cref="Activity"/>
        /// </summary>
        /// <param name="activity">Contains information needed to create an <see cref="Activity"/></param>
        /// <returns><see cref="InfoActivityModel"/> in JSON format with status code 200 on successful creation,
        /// otherwise <see cref="BadRequestObjectResult"/> with error message
        /// </returns>
        [HttpPost]
        public IActionResult Post(CreationActivityModel activity)
        {
            Activity resultActivity;
            try
            {
                resultActivity = _activityRepository.Create(_mapper.Map<Activity>(activity));
            }
            catch (RepositoryException e)
            {
                return BadRequest(e.Message);
            }
            catch (ValidationException e)
            {
                return BadRequest(e.Message);
            }
            return new JsonResult(_mapper.Map<InfoActivityModel>(resultActivity));
        }

        /// <summary>
        /// Gets an <see cref="Activity"/>
        /// </summary>
        /// <param name="id"><see cref="Activity"/>'s id which is needed to be obtained by</param>
        /// <returns><see cref="OkResult"/> on successful deletion, otherwise <see cref="NotFoundObjectResult"/> with error message</returns>
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            Activity resultActivity;
            try
            {
                resultActivity = _activityRepository.GetById(id);
            }
            catch (RepositoryException e)
            {
                return NotFound(e.Message);
            }
            return new JsonResult(_mapper.Map<InfoActivityModel>(resultActivity));
        }

        /// <summary>
        /// Gets all <see cref="Activity"/>s created by specified <see cref="User"/>
        /// </summary>
        /// <param name="userId"><see cref="User"/>'s id which is needed for obtaining collection of <see cref="Activity"/></param>
        /// <returns><see cref="IEnumerable{Activity}"/> or empty collection if <see cref="User"/> exists,
        /// otherwise <see cref="NotFoundObjectResult"/> with error message</returns>
        [HttpGet("user/{userId}")]
        public IActionResult GetByUserId(int userId)
        {
            try
            {
                return new JsonResult(_activityRepository.GetByUserId(userId).Select(a => _mapper.Map<InfoActivityModel>(a)));
            }
            catch (RepositoryException e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Updates an <see cref="Activity"/>
        /// </summary>
        /// <param name="id"><see cref="Activity"/>'s id which is needed to be updated by</param>
        /// <param name="activity">Contains information for updating <see cref="Activity"/></param>
        /// <returns><see cref="InfoActivityModel"/> in JSON format with status code 200 on successful update,
        /// otherwise <see cref="BadRequestObjectResult"/> with error message</returns>
        [HttpPut("{id}")]
        public IActionResult Put(int id, UpdateActivityModel activity)
        {
            Activity resultActivity;
            try
            {
                resultActivity = _activityRepository.Update(id, _mapper.Map<Activity>(activity));
            }
            catch (RepositoryException e)
            {
                return BadRequest(e.Message);
            }
            return new JsonResult(_mapper.Map<InfoActivityModel>(resultActivity));
        }

        /// <summary>
        /// Deletes an existing <see cref="Activity"/>
        /// </summary>
        /// <param name="id"><see cref="Activity"/>'s id which is needed to be deleted by</param>
        /// <returns><see cref="OkResult"/> if <see cref="Activity"/> was successfuly deleted, 
        /// otherwise <see cref="NotFoundObjectResult"/> with error message</returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _activityRepository.Delete(id);
            }
            catch (RepositoryException e)
            {
                return NotFound(e.Message);
            }
            return Ok();
        }

    }
}
