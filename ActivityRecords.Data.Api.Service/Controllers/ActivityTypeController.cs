﻿using Microsoft.AspNetCore.Mvc;
using ActivityRecords.Data.Api.Model.ActivityType;
using ActivityRecords.Data.Repository.Model.Interfaces;
using ActivityRecords.Data.Repository.Model.Entities;
using ActivityRecords.Data.Repository.Model.Exceptions;
using System.Linq;
using AutoMapper;

namespace ActivityRecords.Data.Api.Service.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ActivityTypeController : Controller
    {
        private IActivityTypeRepository _activityTypeRepository;
        private IMapper _mapper;

        public ActivityTypeController(IActivityTypeRepository activityTypeRepository, IMapper mapper)
        {
            _activityTypeRepository = activityTypeRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets an existing <see cref="ActivityType"/>
        /// </summary>
        /// <param name="id"><see cref="ActivityType"/>'s id which is needed to be obtained by</param>
        /// <returns><see cref="InfoActivityTypeModel"/> in JSON format with code 200 if <see cref="ActivityType"/> exists,
        /// otherwise <see cref="NotFoundObjectResult"/></returns>
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            ActivityType activityType;
            try
            {
                activityType = _activityTypeRepository.GetById(id);
            }
            catch (RepositoryException e)
            {
                return NotFound(e.Message);
            }
            return new JsonResult(_mapper.Map<InfoActivityTypeModel>(activityType));
        }

        /// <summary>
        /// Creates a new <see cref="ActivityType"/>
        /// </summary>
        /// <param name="activityType">Contains information needed to create an <see cref="ActivityType"/></param>
        /// <returns><see cref="InfoActivityTypeModel"/> in JSON format on successful creattion, otherwise <see cref="BadRequestObjectResult"/></returns>
        [HttpPost]
        public IActionResult Post(CreationActivityTypeModel activityType)
        {
            ActivityType resultActivityType;
            try
            {
                resultActivityType = _activityTypeRepository.Create(_mapper.Map<ActivityType>(activityType));
            }
            catch (RepositoryException e)
            {
                return BadRequest(e.Message);
            }
            catch (ValidationException e)
            {
                return BadRequest(e.Message);
            }
            return new JsonResult(_mapper.Map<InfoActivityTypeModel>(resultActivityType));
        }

        /// <summary>
        /// Updates an <see cref="ActivityType"/>
        /// </summary>
        /// <param name="id"><see cref="ActivityType"/>'s id which is needed to be updated by</param>
        /// <param name="activityType">Contains information for <see cref="ActivityType"/> to be updated</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public IActionResult Put(int id, UpdateActivityTypeModel activityType)
        {
            ActivityType resultActivityType;
            try
            {
                resultActivityType = _activityTypeRepository.Update(id, _mapper.Map<ActivityType>(activityType));
            }
            catch (RepositoryException e)
            {
                return BadRequest(e.Message);
            }
            catch (ValidationException e)
            {
                return BadRequest(e.Message);
            }
            return new JsonResult(_mapper.Map<InfoActivityTypeModel>(resultActivityType));
        }

        /// <summary>
        /// Deletes an <see cref="ActivityType"/>
        /// </summary>
        /// <param name="id"><see cref="ActivityType"/>'s id which is needed to be deleted by</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _activityTypeRepository.Delete(id);
            }
            catch (RepositoryException e)
            {
                return NotFound(e.Message);
            }
            return Ok();
        }

        [HttpGet("user/{userId}")]
        public IActionResult GetByUserId(int userId)
        {
            JsonResult resultList;
            try
            {
                resultList = new JsonResult(_activityTypeRepository
                    .GetByUserId(userId)
                    .Select(at => _mapper.Map<InfoActivityTypeModel>(at)));
            }
            catch (RepositoryException e)
            {
                return NotFound(e.Message);
            }
            return resultList;
        }
    }
}
