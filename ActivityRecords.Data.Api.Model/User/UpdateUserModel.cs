﻿using System;

namespace ActivityRecords.Data.Api.Model.User
{
    public class UpdateUserModel
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public DateTime? BirthDate { get; set; }
        public float? Weight { get; set; }
        public int? Height { get; set; }
    }
}
