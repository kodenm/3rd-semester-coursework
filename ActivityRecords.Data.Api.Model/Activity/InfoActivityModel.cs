﻿using ActivityRecords.Data.Api.Model.ActivityDetail;
using System;
using System.Collections.Generic;

namespace ActivityRecords.Data.Api.Model.Activity
{
    public class InfoActivityModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int ActivityTypeId { get; set; }
        public DateTime DateTime { get; set; }
        public IEnumerable<ActivityDetailModel> Details { get; set; }
    }
}
