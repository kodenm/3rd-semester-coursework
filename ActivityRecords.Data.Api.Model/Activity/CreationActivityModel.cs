﻿using ActivityRecords.Data.Api.Model.ActivityDetail;
using System;
using System.Collections.Generic;

namespace ActivityRecords.Data.Api.Model.Activity
{
    public class CreationActivityModel
    {
        public int ActivityTypeId { get; set; }
        public int UserId { get; set; }
        public DateTime DateTime { get; set; }
        public IEnumerable<ActivityDetailModel> Details { get; set; }
    }
}
