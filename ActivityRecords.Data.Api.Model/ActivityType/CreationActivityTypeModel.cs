﻿using System.Collections.Generic;

namespace ActivityRecords.Data.Api.Model.ActivityType
{
    public class CreationActivityTypeModel
    {
        public string Name { get; set; }
        public int UserId { get; set; }
        public IEnumerable<string> DetailNames { get; set; }
    }
}
