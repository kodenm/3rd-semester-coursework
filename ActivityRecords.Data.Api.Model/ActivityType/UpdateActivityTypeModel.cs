﻿using System.Collections.Generic;

namespace ActivityRecords.Data.Api.Model.ActivityType
{
    public class UpdateActivityTypeModel
    {
        public string Name { get; set; }
        public IEnumerable<string> DetailNames { get; set; }
    }
}
