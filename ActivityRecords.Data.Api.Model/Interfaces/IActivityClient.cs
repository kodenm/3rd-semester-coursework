﻿using ActivityRecords.Data.Api.Model.Activity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ActivityRecords.Data.Api.Model.Interfaces
{
    public interface IActivityClient
    {
        Task<InfoActivityModel> GetById(int id);
        Task<IEnumerable<InfoActivityModel>> GetByUserId(int userId);
        Task<InfoActivityModel> Create(CreationActivityModel creationActivity);
        Task<InfoActivityModel> Update(int id, UpdateActivityModel updateActivity);
        Task Delete(int id);
    }
}
