﻿using ActivityRecords.Data.Api.Model.User;
using ActivityRecords.Data.Repository.Model.Entities;
using ActivityRecords.Data.Repository.Model.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;

namespace ActivityRecords.Data.Api.Service.Tests.UserControllerTests
{
    public partial class UserControllerTests
    {
        [Test]
        public void ShouldReturnUserInfoOnCreateWithCorrectData()
        {
            // arrange
            CreationUserModel inputUser = new CreationUserModel
            {
                Id = 1,
                Login = "testlogin1"
            };
            User createdUser = new User
            {
                Id = 1,
                Login = inputUser.Login,
                RegistrationDateTime = DateTime.Now
            };
            _userRepositoryMock.Setup(urm => urm.Create(It.IsAny<User>()))
                .Returns(createdUser);

            // act
            var returnedUser = (_userController.Create(inputUser) as JsonResult)?.Value as InfoUserModel;

            // assert
            Assert.IsNotNull(returnedUser);
            UsersAreEqual(createdUser, returnedUser);
        }

        [Test]
        public void ShouldReturnBadRequestOnCreateWhenRepositoryExceptionThrown()
        {
            // arrange
            CreationUserModel inputUser = new CreationUserModel
            {
                Id = 1,
                Login = "existinglogin1"
            };
            _userRepositoryMock.Setup(urm => urm.Create(It.IsAny<User>()))
                .Throws(new RepositoryException("login already taken"));

            // act
            var contentResult = _userController.Create(inputUser) as BadRequestObjectResult;

            // assert
            Assert.IsNotNull(contentResult);
            Assert.AreEqual(contentResult.Value, "login already taken");
        }

        public void ShouldReturnBadRequestOnCreateWhenValidationExceptionThrown()
        {
            // arrange
            CreationUserModel inputUser = new CreationUserModel
            {
                Id = 1,
                Login = "testlogin1"
            };
            _userRepositoryMock.Setup(urm => urm.Create(It.IsAny<User>()))
                .Throws(new ValidationException("required data contains nulls"));

            // act
            var contentResult = _userController.Create(inputUser) as BadRequestObjectResult;

            // assert
            Assert.IsNotNull(contentResult);
            Assert.AreEqual(contentResult.Value, "required data contains nulls");
        }
    }
}
