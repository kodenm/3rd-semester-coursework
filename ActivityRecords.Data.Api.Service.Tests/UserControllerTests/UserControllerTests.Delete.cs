﻿using ActivityRecords.Data.Repository.Model.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace ActivityRecords.Data.Api.Service.Tests.UserControllerTests
{
    public partial class UserControllerTests
    {
        [Test]
        public void ShouldReturnOkOnDeleteWithCorrectData()
        {
            // arrange
            int existingUserId = 1;
            _userRepositoryMock.Setup(urm => urm.Delete(It.IsAny<int>()));

            // act
            var contentResult = _userController.Delete(existingUserId) as OkResult;

            // assert
            Assert.IsNotNull(contentResult);
        }

        [Test]
        public void ShouldReturnNotFoundOnDeleteWhenRepositoryExceptionThrown()
        {
            // arrange
            int nonExistingUserId = 1;
            _userRepositoryMock.Setup(urm => urm.Delete(It.IsAny<int>()))
                .Throws(new RepositoryException("no user with this id"));

            // act
            var contentResult = _userController.Delete(nonExistingUserId) as NotFoundObjectResult;

            // assert
            Assert.IsNotNull(contentResult);
            Assert.AreEqual(contentResult.Value, "no user with this id");
        }
    }
}
