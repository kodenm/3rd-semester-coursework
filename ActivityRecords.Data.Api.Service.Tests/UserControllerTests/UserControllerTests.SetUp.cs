﻿using ActivityRecords.Data.Api.Model.User;
using ActivityRecords.Data.Api.Service.Controllers;
using ActivityRecords.Data.Repository.Model.Entities;
using ActivityRecords.Data.Repository.Model.Interfaces;
using Moq;
using NUnit.Framework;

namespace ActivityRecords.Data.Api.Service.Tests.UserControllerTests
{
    [TestFixture]
    partial class UserControllerTests : TestBase
    {
        private UserController _userController;
        
        [SetUp]
        public void TestControllerSetUp()
        {
            _userRepositoryMock = new Mock<IUserRepository>();
            _userController = new UserController(_userRepositoryMock.Object, _mapper);
        }

        private void UsersAreEqual(User inputUser, InfoUserModel outputUser)
        {
            Assert.AreEqual(inputUser.Login, outputUser.Login);
            Assert.AreEqual(inputUser.RegistrationDateTime, outputUser.RegistrationDateTime);
            Assert.AreEqual(inputUser.BirthDate, outputUser.BirthDate);
            Assert.AreEqual(inputUser.Weight, outputUser.Weight);
            Assert.AreEqual(inputUser.Height, outputUser.Height);
        }
    }
}
