﻿using ActivityRecords.Data.Api.Model.ActivityType;
using ActivityRecords.Data.Repository.Model.Entities;
using ActivityRecords.Data.Repository.Model.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace ActivityRecords.Data.Api.Service.Tests.ActivityTypeControllerTests
{
    public partial class ActivityTypeControllerTests
    {
        [Test]
        public void ShouldReturnActivityTypeInfoOnUpdateWithCorrectData()
        {
            // arrange
            int activityTypeId = 1;
            int userId = 1;
            UpdateActivityTypeModel inputActivityType = new UpdateActivityTypeModel
            {
                Name = "TestActivityName1",
                DetailNames = new[]
                {
                    "TestDetail1",
                    "TestDetail2"
                }
            };
            ActivityType expectedActivityType = new ActivityType
            {
                Id = activityTypeId,
                UserId = userId,
                Name = "TestActivityName1",
                DetailNames = new[]
                {
                    "TestDetail1",
                    "TestDetail2"
                }
            };
            _activityTypeRepositoryMock.Setup(atrm => atrm.Update(It.IsAny<int>(), It.IsAny<ActivityType>()))
                .Returns(expectedActivityType);

            // act
            var returnedActivityType = (_activityTypeController.Put(activityTypeId, inputActivityType) as JsonResult)?.Value as InfoActivityTypeModel;

            // assert
            Assert.IsNotNull(returnedActivityType);
            AssertActivityTypesAreEqual(expectedActivityType, returnedActivityType);
        }

        [Test]
        public void ShouldReturnBadRequestOnUpdateWhenRepositoryExceptionThrown()
        {
            // arrange
            int activityTypeId = 1;
            UpdateActivityTypeModel inputActivityType = new UpdateActivityTypeModel
            {
                Name = "TestActivityName1",
                DetailNames = new[]
                {
                    "TestDetail1",
                    "TestDetail2"
                }
            };
            _activityTypeRepositoryMock.Setup(atrm => atrm.Update(It.IsAny<int>(), It.IsAny<ActivityType>()))
                .Throws(new RepositoryException("error during activity type update"));

            // act
            var returnedActivityType = _activityTypeController.Put(activityTypeId, inputActivityType) as BadRequestObjectResult;

            // assert
            Assert.IsNotNull(returnedActivityType);
            Assert.AreEqual(returnedActivityType.Value, "error during activity type update");
        }

        [Test]
        public void ShouldReturnBadRequestOnUpdateWhenValidationExceptionThrown()
        {
            // arrange
            int activityTypeId = 1;
            UpdateActivityTypeModel inputActivityType = new UpdateActivityTypeModel
            {
                Name = "TestActivityName1",
                DetailNames = new[]
                {
                    "TestDetail1",
                    "TestDetail2"
                }
            };
            _activityTypeRepositoryMock.Setup(atrm => atrm.Update(It.IsAny<int>(), It.IsAny<ActivityType>()))
                .Throws(new ValidationException("error during activity type update"));

            // act
            var returnedActivityType = _activityTypeController.Put(activityTypeId, inputActivityType) as BadRequestObjectResult;

            // assert
            Assert.IsNotNull(returnedActivityType);
            Assert.AreEqual(returnedActivityType.Value, "error during activity type update");
        }
    }
}
