﻿using ActivityRecords.Data.Api.Model.ActivityType;
using ActivityRecords.Data.Repository.Model.Entities;
using ActivityRecords.Data.Repository.Model.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace ActivityRecords.Data.Api.Service.Tests.ActivityTypeControllerTests
{
    public partial class ActivityTypeControllerTests
    {
        [Test]
        public void ShouldReturnActivityTypeInfoOnCreateWithCorrectData()
        {
            // arrange
            int activityTypeId = 1;
            int userId = 1;
            string name = "TestActivityName1";
            CreationActivityTypeModel inputActivityType = new CreationActivityTypeModel
            {
                Name = name,
                UserId = userId
            };
            ActivityType expectedResult = new ActivityType
            {
                Id = activityTypeId,
                Name = name,
                UserId = userId
            };
            _activityTypeRepositoryMock.Setup(atrm => atrm.Create(It.IsAny<ActivityType>()))
                .Returns(expectedResult);

            // act
            var returnedActivityType = (_activityTypeController.Post(inputActivityType) as JsonResult)?.Value as InfoActivityTypeModel;

            // assert
            Assert.IsNotNull(returnedActivityType);
            AssertActivityTypesAreEqual(expectedResult, returnedActivityType);
        }

        [Test]
        public void ShouldReturnBadRequestOnCreateWhenRepositoryExceptionThrown()
        {
            // arrange
            CreationActivityTypeModel inputActivityType = new CreationActivityTypeModel();
            _activityTypeRepositoryMock.Setup(atrm => atrm.Create(It.IsAny<ActivityType>()))
                .Throws(new RepositoryException("repository exception"));

            // act
            var returnedActivityType = _activityTypeController.Post(inputActivityType) as BadRequestObjectResult;

            // assert
            Assert.IsNotNull(returnedActivityType);

        }

        [Test]
        public void ShouldReturnBadRequestOnCreateWhenValidationExceptionThrown()
        {
            // arrange
            CreationActivityTypeModel inputActivityType = new CreationActivityTypeModel();
            _activityTypeRepositoryMock.Setup(atrm => atrm.Create(It.IsAny<ActivityType>()))
                .Throws(new ValidationException("validation exception"));

            // act
            var returnedActivityType = _activityTypeController.Post(inputActivityType) as BadRequestObjectResult;

            // assert
            Assert.IsNotNull(returnedActivityType);
        }
    }
}
