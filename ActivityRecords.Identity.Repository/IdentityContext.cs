﻿using ActivityRecords.Identity.Model.Entities;
using ActivityRecords.Identity.Repository.Configurations;
using Microsoft.EntityFrameworkCore;

namespace ActivityRecords.Identity.Repository
{
    public class IdentityContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        public IdentityContext(DbContextOptions<IdentityContext> dbContextOptions) : base(dbContextOptions)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfiguration());
        }
    }
}
