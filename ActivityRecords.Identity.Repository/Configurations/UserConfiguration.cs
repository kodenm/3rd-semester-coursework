﻿using ActivityRecords.Identity.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ActivityRecords.Identity.Repository.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(u => u.Id);
            builder.Property(u => u.Login).IsRequired();
            builder.HasIndex(u => u.Login).IsUnique();
            builder.Property(u => u.PasswordHash).IsRequired();
        }
    }
}
