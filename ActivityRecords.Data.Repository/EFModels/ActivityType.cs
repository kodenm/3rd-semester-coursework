﻿using System.Collections.Generic;

namespace ActivityRecords.Data.Repository.EFModels
{
    public class ActivityType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public IEnumerable<ActivityTypeDetail> ActivityTypeDetails { get; set; }
        public IEnumerable<Activity> Activities { get; set; }
    }
}
