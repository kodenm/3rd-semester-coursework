﻿namespace ActivityRecords.Data.Repository.EFModels
{
    public class ActivityTypeDetail
    {
        public ActivityType ActivityType { get; set; }
        public int ActivityTypeId { get; set; }
        public string DetailName { get; set; }
    }
}
