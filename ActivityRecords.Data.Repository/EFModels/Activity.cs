﻿using System;
using System.Collections.Generic;

namespace ActivityRecords.Data.Repository.EFModels
{
    public class Activity
    {
        public int Id { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }
        public ActivityType ActivityType { get; set; }
        public int ActivityTypeId { get; set; }
        public DateTime Date { get; set; }
        public IEnumerable<ActivityDetail> ActivityDetails { get; set; }
    }
}
