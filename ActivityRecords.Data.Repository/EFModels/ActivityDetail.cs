﻿namespace ActivityRecords.Data.Repository.EFModels
{
    public class ActivityDetail
    {
        public Activity Activity { get; set; }
        public int ActivityId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
