﻿using System.Linq;
using ActivityRecords.Data.Repository.Model.Exceptions;
using ActivityRecords.Data.Repository.Model.Interfaces;
using AutoMapper;
using EFUser = ActivityRecords.Data.Repository.EFModels.User;
using ModelUser = ActivityRecords.Data.Repository.Model.Entities.User;

namespace ActivityRecords.Data.Repository.Repositories
{
    public class UserRepository : IUserRepository
    {
        private DataContext _context;
        private IMapper _mapper;
        public UserRepository(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public ModelUser Create(ModelUser modelUser)
        {
            if (string.IsNullOrEmpty(modelUser.Login))
            {
                throw new ValidationException("No login");
            }
            if (_context.Users.FirstOrDefault(u => u.Login == modelUser.Login) != null)
            {
                throw new RepositoryException("User with specified login already exists");
            }
            modelUser.RegistrationDateTime = System.DateTime.UtcNow;
            var efUser = _context.Users.Add(_mapper.Map<ModelUser, EFUser>(modelUser)).Entity;
            _context.SaveChanges();
            return _mapper.Map(efUser, modelUser);
        }

        public void Delete(int id)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id == id);
            if (user == null)
            {
                throw new RepositoryException("User with specified id not found");
            }
            _context.Users.Remove(user);
            _context.SaveChanges();
        }

        public ModelUser GetById(int id)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id == id);
            if (user == null)
            {
                throw new RepositoryException("User with specified id not found");
            }
            return _mapper.Map<ModelUser>(user);
        }

        public ModelUser GetByLogin(string login)
        {
            var user = _context.Users.FirstOrDefault(u => u.Login == login);
            if (login == null)
            {
                throw new ValidationException("No login provided");
            }
            if (user == null)
            {
                throw new RepositoryException("User with specified login not found");
            }
            return _mapper.Map<ModelUser>(_context.Users.FirstOrDefault(u => u.Login == login));
        }

        public ModelUser Update(int id, ModelUser modelUser)
        {
            var efUser = _context.Users.FirstOrDefault(u => u.Id == id);
            if (efUser == null)
            {
                throw new RepositoryException("User with specified ID not found");
            }
            if (_context.Users.FirstOrDefault(u => u.Id != id && u.Login == modelUser.Login) != null)
            {
                throw new RepositoryException("User with specified login already exists");
            }
            UpdateFromModel(modelUser, efUser);
            _context.SaveChanges();
            return _mapper.Map(efUser, modelUser);
        }

        private void UpdateFromModel(ModelUser source, EFUser destination)
        {
            destination.Login = string.IsNullOrEmpty(source.Login) ? destination.Login : source.Login;
            destination.BirthDate = source.BirthDate;
            destination.Weight = source.Weight;
            destination.Height = source.Height;
        }
    }
}
