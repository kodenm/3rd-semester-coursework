﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using ActivityRecords.Data.Repository.Model.Interfaces;
using EFActivityType = ActivityRecords.Data.Repository.EFModels.ActivityType;
using ModelActivityType = ActivityRecords.Data.Repository.Model.Entities.ActivityType;
using ActivityRecords.Data.Repository.Model.Exceptions;
using AutoMapper;
using ActivityRecords.Data.Repository.EFModels;

namespace ActivityRecords.Data.Repository.Repositories
{
    public class ActivityTypeRepository : IActivityTypeRepository
    {
        public DataContext _context;
        private IMapper _mapper;

        public ActivityTypeRepository(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public ModelActivityType Create(ModelActivityType modelActivityType)
        {
            if (string.IsNullOrEmpty(modelActivityType.Name))
            {
                throw new ValidationException("No name provided");
            }
            if (_context.Users.FirstOrDefault(u => u.Id == modelActivityType.UserId) == null)
            {
                throw new RepositoryException("Specified by id user not found");
            }
            if (_context.ActivityTypes.FirstOrDefault(at => at.Name == modelActivityType.Name && at.UserId == modelActivityType.UserId) != null)
            {
                throw new RepositoryException("User already contains an activity type with the same name");
            }
            var efActivityType = _context.ActivityTypes.Add(_mapper.Map<ModelActivityType, EFActivityType>(modelActivityType)).Entity;
            _context.SaveChanges();
            modelActivityType.Id = efActivityType.Id;
            if (modelActivityType.DetailNames != null)
            {
                _context.ActivityTypeDetails.AddRange(_mapper.Map<ModelActivityType, IEnumerable<ActivityTypeDetail>>(modelActivityType));
                _context.SaveChanges();
            }
            return _mapper.Map<EFActivityType, ModelActivityType>(efActivityType);
        }

        public void Delete(int id)
        {
            if (_context.ActivityTypes.FirstOrDefault(at => at.Id == id) == null)
            {
                throw new RepositoryException("Activity Type with specified id not found");
            }
            var activity = _context.ActivityTypes.Include(at => at.ActivityTypeDetails).FirstOrDefault(at => at.Id == id);
            _context.ActivityTypeDetails.RemoveRange(activity.ActivityTypeDetails);
            _context.ActivityTypes.Remove(activity);
            _context.SaveChanges();
        }

        public ModelActivityType GetById(int id)
        {
            var activityType = _context.ActivityTypes.Include(at => at.ActivityTypeDetails).FirstOrDefault(at => at.Id == id);
            if (activityType == null)
            {
                throw new RepositoryException("Activity Type with specified id not found");
            }
            return _mapper.Map<EFActivityType, ModelActivityType>(activityType);
        }

        public IEnumerable<ModelActivityType> GetByUserId(int userId)
        {
            if (_context.Users.FirstOrDefault(u => u.Id == userId) == null)
            {
                throw new RepositoryException("Specified by id user not found");
            }
            return _context.ActivityTypes
                .Where(at => at.UserId == userId)
                .Include(at => at.ActivityTypeDetails)
                .Select(at => _mapper.Map<EFActivityType, ModelActivityType>(at));
        }

        public ModelActivityType Update(int id, ModelActivityType modelActivityType)
        {
            EFActivityType efActivityType = _context.ActivityTypes.Include(at => at.ActivityTypeDetails).FirstOrDefault(at => at.Id == id);
            if (efActivityType == null)
            {
                throw new RepositoryException("Activity Type specified by id not found");
            }
            if (_context.ActivityTypes.FirstOrDefault(at => at.Id != id && modelActivityType.UserId == at.UserId && at.Name == modelActivityType.Name) != null)
            {
                throw new RepositoryException("User already has an activity type with this name");
            }
            UpdateFromModel(efActivityType, modelActivityType);
            _context.SaveChanges();
            _context.ActivityTypeDetails.RemoveRange(_context.ActivityTypeDetails.Where(atd => atd.ActivityTypeId == id));
            if (modelActivityType.DetailNames != null)
            {
                _context.ActivityTypeDetails.AddRange(modelActivityType.DetailNames.Select(d => new EFModels.ActivityTypeDetail
                {
                    ActivityTypeId = id,
                    DetailName = d
                }));
                ;
                foreach (var activity in _context.Activities.Include(a => a.ActivityDetails).Where(a => a.ActivityTypeId == efActivityType.Id))
                {
                    _context.ActivityDetails.RemoveRange(activity.ActivityDetails.Where(ad => !modelActivityType.DetailNames.Contains(ad.Name)));
                    _context.ActivityDetails.AddRange(modelActivityType.DetailNames
                        .Where(d => activity.ActivityDetails.FirstOrDefault(ad => ad.Name == d) == null)
                        .Select(d => new ActivityDetail { ActivityId = activity.Id, Name = d, Value = null }));
                }
            }
            else
            {
                _context.ActivityDetails.RemoveRange(_context.ActivityDetails
                    .Where(ad => _context.Activities.FirstOrDefault(a => a.Id == ad.ActivityId && a.ActivityTypeId == id) != null));
            }
            _context.SaveChanges();
            return _mapper.Map<EFActivityType, ModelActivityType>(efActivityType);
        }

        private protected void UpdateFromModel(EFActivityType destination, ModelActivityType source)
        {
            destination.Name = source.Name ?? destination.Name;
        }

    }
}
