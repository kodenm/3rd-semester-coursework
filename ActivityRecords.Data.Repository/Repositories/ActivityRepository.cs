﻿using System.Collections.Generic;
using System.Linq;
using ActivityRecords.Data.Repository.Model.Exceptions;
using ActivityRecords.Data.Repository.Model.Interfaces;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ModelActivity = ActivityRecords.Data.Repository.Model.Entities.Activity;
using EFActivity = ActivityRecords.Data.Repository.EFModels.Activity;
using ModelActivityDetail = ActivityRecords.Data.Repository.Model.Entities.ActivityDetail;
using EFActivityDetail = ActivityRecords.Data.Repository.EFModels.ActivityDetail;

namespace ActivityRecords.Data.Repository.Repositories
{
    public class ActivityRepository : IActivityRepository
    {
        private DataContext _context;
        private IMapper _mapper;

        public ActivityRepository(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public ModelActivity Create(ModelActivity modelActivity)
        {
            if (_context.Users.FirstOrDefault(u => u.Id == modelActivity.UserId) == null)
            {
                throw new RepositoryException("Specified user not found");
            }
            if (_context.ActivityTypes.FirstOrDefault(a => a.Id == modelActivity.ActivityTypeId) == null)
            {
                throw new RepositoryException("Specified activity type not found");
            }
            if (_context.ActivityTypes.FirstOrDefault(at => at.Id == modelActivity.ActivityTypeId).UserId != modelActivity.UserId)
            {
                throw new RepositoryException("Specified user doesn't have specified activity type");
            }
            _context.Activities.Include(a => a.ActivityDetails);
            var efActivity = _context.Activities.Add(_mapper.Map<ModelActivity, EFActivity>(modelActivity)).Entity;
            _context.SaveChanges();
            return _mapper.Map<ModelActivity>(efActivity);
        }

        public void Delete(int id)
        {
            var activity = _context.Activities.FirstOrDefault(a => a.Id == id);
            if (activity == null)
            {
                throw new RepositoryException("Specified Activity doesn't exist");
            }
            _context.Activities.Remove(activity);
            _context.SaveChanges();
        }

        public ModelActivity GetById(int id)
        {
            var activity = _context.Activities.Include(a => a.ActivityDetails).FirstOrDefault(a => a.Id == id);
            if (activity == null)
            {
                throw new RepositoryException("Activity with specified id doesn't exist");
            }
            return _mapper.Map<ModelActivity>(activity);
        }

        public IEnumerable<ModelActivity> GetByUserId(int userId)
        {
            if (_context.Users.FirstOrDefault(u => u.Id == userId) == null)
            {
                throw new RepositoryException("Specified User doesn't exist");
            }
            return _context.Activities.Include(a => a.ActivityDetails)
                .Where(a => a.UserId == userId)
                .Select(a => _mapper.Map<ModelActivity>(a));
        }

        public IEnumerable<ModelActivity> GetByUserIdAndType(IActivityFilter filter)
        {
            return _context.Activities.Include(a => a.ActivityDetails)
                .Where(a => filter.FitsCreteria(_mapper.Map<ModelActivity>(a)))
                .Select(a => _mapper.Map<ModelActivity>(a));
        }

        public ModelActivity Update(int id, ModelActivity modelActivity)
        {
            var efActivity = _context.Activities.Include(a => a.ActivityDetails).FirstOrDefault(a => a.Id == id);
            var activityTypeDetails = _context.ActivityTypeDetails.Where(atd => atd.ActivityTypeId == efActivity.ActivityTypeId);
            if (efActivity == null)
            {
                throw new RepositoryException("Activity doesn't exist");
            }
            if (modelActivity.Details != null && Enumerable.DefaultIfEmpty(activityTypeDetails) != null &&
                !Enumerable.SequenceEqual(modelActivity.Details.Select(d => d.Name).OrderBy(d => d), 
                activityTypeDetails.Select(atd => atd.DetailName).OrderBy(d => d)))
            {
                throw new RepositoryException("List of details from updated activity doesn't match ones from activity type");
            }
            efActivity.ActivityDetails = _mapper.Map<IEnumerable<ModelActivityDetail>, IEnumerable<EFActivityDetail>>(modelActivity.Details);
            _context.SaveChanges();
            return _mapper.Map<ModelActivity>(efActivity);
        }
    }
}
