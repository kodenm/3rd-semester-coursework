﻿using ActivityRecords.Data.Repository.EFModels;
using AutoMapper;
using ModelActivityType = ActivityRecords.Data.Repository.Model.Entities.ActivityType;
using System.Collections.Generic;
using System.Linq;

namespace ActivityRecords.Data.Repository.MapperProfiles
{
    public class ActivityTypeDetailProfile : Profile
    {
        public ActivityTypeDetailProfile()
        {
            CreateMap<ModelActivityType, IEnumerable<ActivityTypeDetail>>()
                .ConvertUsing(d => d.DetailNames.Select(dn => new ActivityTypeDetail { ActivityTypeId = d.Id, DetailName = dn }));
        }
    }
}
