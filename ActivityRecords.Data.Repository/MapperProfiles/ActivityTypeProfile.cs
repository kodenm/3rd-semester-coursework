﻿using AutoMapper;
using System.Linq;
using EFActivityType = ActivityRecords.Data.Repository.EFModels.ActivityType;
using ModelActivityType = ActivityRecords.Data.Repository.Model.Entities.ActivityType;

namespace ActivityRecords.Data.Repository.MapperProfiles
{
    public class ActivityTypeProfile : Profile
    {
        public ActivityTypeProfile()
        {
            CreateMap<EFActivityType, ModelActivityType>()
                .ForMember(d => d.Id, memberOptions => memberOptions.MapFrom(s => s.Id))
                .ForMember(d => d.Name, memberOptions => memberOptions.MapFrom(s => s.Name))
                .ForMember(d => d.UserId, memberOptions => memberOptions.MapFrom(s => s.UserId))
                .ForMember(d => d.DetailNames, memberOptions => memberOptions.MapFrom(s => s.ActivityTypeDetails.Select(atd => atd.DetailName)));

            CreateMap<ModelActivityType, EFActivityType>()
                .ForMember(activitytype => activitytype.Id, memberOptions => memberOptions.MapFrom(activityType => activityType.Id))
                .ForMember(activitytype => activitytype.UserId, memberOptions => memberOptions.MapFrom(activityType => activityType.UserId))
                .ForMember(activitytype => activitytype.Name, memberOptions => memberOptions.MapFrom(activityType => activityType.Name));
        }
    }
}
