﻿using ActivityRecords.Data.Repository.EFModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ActivityRecords.Data.Repository.Configurations
{
    public class ActivityTypeDetailEntityTypeConfiguration : IEntityTypeConfiguration<ActivityTypeDetail>
    {
        public void Configure(EntityTypeBuilder<ActivityTypeDetail> builder)
        {
            builder.ToTable("ActivityTypeDetail");
            builder.HasKey(td => new { td.ActivityTypeId, td.DetailName });
            builder.Property(atd => atd.DetailName).HasMaxLength(50);
        }
    }
}
