﻿using ActivityRecords.Data.Repository.EFModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ActivityRecords.Data.Repository.Configurations
{
    public class UserEntityTypeConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("User");
            builder.HasKey(user => user.Id);
            builder.Property(user => user.Id).ValueGeneratedNever();
            builder.HasIndex(user => user.Login).IsUnique();
            builder.Property(u => u.Login).IsRequired().HasMaxLength(50);
            builder.Property(u => u.RegistrationDateTime).IsRequired();
            builder.HasMany(u => u.Activities).WithOne(a => a.User).HasForeignKey(a => a.UserId).OnDelete(DeleteBehavior.NoAction);
            builder.HasMany(u => u.ActivityTypes).WithOne(at => at.User).HasForeignKey(at => at.UserId);
            builder.Property(u => u.BirthDate).HasColumnType("date");
        }
    }
}
