﻿namespace ActivityRecords.Data.Repository.Model.Entities
{
    public class ActivityDetail
    {
        public int ActivityId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
