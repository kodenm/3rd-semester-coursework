﻿using System;

namespace ActivityRecords.Data.Repository.Model.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public DateTime RegistrationDateTime { get; set; }
        public DateTime? BirthDate { get; set; }
        public float? Weight { get; set; }
        public int? Height { get; set; }
    }
}
