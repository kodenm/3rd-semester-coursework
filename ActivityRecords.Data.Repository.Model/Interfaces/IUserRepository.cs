﻿using ActivityRecords.Data.Repository.Model.Entities;

namespace ActivityRecords.Data.Repository.Model.Interfaces
{
    public interface IUserRepository
    {
        public User GetById(int id);
        public User GetByLogin(string login);
        User Create(User user);
        User Update(int id, User user);
        void Delete(int id);
    }
}
