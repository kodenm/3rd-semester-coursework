﻿using ActivityRecords.Data.Repository.Model.Entities;
using System.Collections.Generic;

namespace ActivityRecords.Data.Repository.Model.Interfaces
{
    public interface IActivityTypeRepository
    {
        public ActivityType GetById(int id);
        public IEnumerable<ActivityType> GetByUserId(int userId);
        ActivityType Create(ActivityType activity);
        ActivityType Update(int id, ActivityType activity);
        void Delete(int id);
    }
}
