﻿using ActivityRecords.Data.Repository.Model.Entities;
using System.Collections.Generic;

namespace ActivityRecords.Data.Repository.Model.Interfaces
{
    public interface IActivityRepository
    {
        public Activity GetById(int id);
        public IEnumerable<Activity> GetByUserId(int userId);
        public IEnumerable<Activity> GetByUserIdAndType(IActivityFilter filter);
        Activity Create(Activity activity);
        Activity Update(int id, Activity activity);
        void Delete(int id);
    }
}
