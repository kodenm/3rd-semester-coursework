﻿using System;

namespace ActivityRecords.Data.Repository.Model.Exceptions
{
    public class RepositoryException : Exception
    {
        public RepositoryException(string message) : base(message)
        {

        }
    }
}
