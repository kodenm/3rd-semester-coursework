﻿using System;

namespace ActivityRecords.Data.Repository.Model.Exceptions
{
    public class ValidationException : Exception
    {
        public ValidationException(string message) : base(message)
        {

        }
    }
}
