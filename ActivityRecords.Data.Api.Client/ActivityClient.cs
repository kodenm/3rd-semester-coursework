﻿using ActivityRecords.Client.Common;
using ActivityRecords.Data.Api.Model.Activity;
using ActivityRecords.Data.Api.Model.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ActivityRecords.Data.Api.Client
{
    public class ActivityClient : ClientBase, IActivityClient
    {
        public ActivityClient(Uri serverUri) : base(serverUri) { }
        public async Task<InfoActivityModel> Create(CreationActivityModel creationActivity)
        {
            var response = await PostAsyncInternal("activity", creationActivity);
            if (response == null || !response.IsSuccessStatusCode)
            {
                throw new HttpRequestException(await response.Content.ReadAsStringAsync());
            }
            return await ResponseToObject<InfoActivityModel>(response);
        }

        public async Task Delete(int id)
        {
            var response = await _client.DeleteAsync($"activity/{id}");
            if (response == null || !response.IsSuccessStatusCode)
            {
                throw new HttpRequestException(await response.Content.ReadAsStringAsync());
            }
        }

        public async Task<InfoActivityModel> GetById(int id)
        {
            var response = await _client.GetAsync($"activity/{id}");
            if (response == null || !response.IsSuccessStatusCode)
            {
                throw new HttpRequestException(await response.Content.ReadAsStringAsync());
            }
            return await ResponseToObject<InfoActivityModel>(response);
        }

        public async Task<IEnumerable<InfoActivityModel>> GetByUserId(int userId)
        {
            var response = await _client.GetAsync($"activity/user/{userId}");
            if (response == null || !response.IsSuccessStatusCode)
            {
                throw new HttpRequestException(await response.Content.ReadAsStringAsync());
            }
            return await ResponseToObject<IEnumerable<InfoActivityModel>>(response);
        }

        public async Task<InfoActivityModel> Update(int id, UpdateActivityModel updateActivity)
        {
            var response = await PutAsyncInternal($"activity/{id}", updateActivity);
            if (response == null || !response.IsSuccessStatusCode)
            {
                throw new HttpRequestException(await response.Content.ReadAsStringAsync());
            }
            return await ResponseToObject<InfoActivityModel>(response);
        }
    }
}
