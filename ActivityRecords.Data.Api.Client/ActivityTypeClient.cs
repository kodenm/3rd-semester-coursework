﻿using ActivityRecords.Client.Common;
using ActivityRecords.Data.Api.Model.ActivityType;
using ActivityRecords.Data.Api.Model.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace ActivityRecords.Data.Api.Client
{
    public class ActivityTypeClient : ClientBase, IActivityTypeClient
    {
        public ActivityTypeClient(Uri serverUri) : base(serverUri) { }
        public async Task<InfoActivityTypeModel> Create(CreationActivityTypeModel creationActivityType)
        {
            var response = await PostAsyncInternal("activitytype", creationActivityType);
            if (response == null || !response.IsSuccessStatusCode)
            {
                throw new HttpRequestException(await response.Content.ReadAsStringAsync());
            }
            return await ResponseToObject<InfoActivityTypeModel>(response);
        }

        public async Task Delete(int id)
        {
            var response = await _client.DeleteAsync($"activitytype/{id}");
            if (response == null || !response.IsSuccessStatusCode)
            {
                throw new HttpRequestException(await response.Content.ReadAsStringAsync());
            }
        }

        public async Task<InfoActivityTypeModel> GetById(int id)
        {
            var response = await _client.GetAsync($"activitytype/{id}");
            if (response == null || !response.IsSuccessStatusCode)
            {
                throw new HttpRequestException(await response.Content.ReadAsStringAsync());
            }
            return await ResponseToObject<InfoActivityTypeModel>(response);
        }

        public async Task<IEnumerable<InfoActivityTypeModel>> GetByUserId(int userId)
        {
            var response = await _client.GetAsync($"activitytype/user/{userId}");
            if (response == null || !response.IsSuccessStatusCode)
            {
                throw new HttpRequestException(await response.Content.ReadAsStringAsync());
            }
            return await ResponseToObject<IEnumerable<InfoActivityTypeModel>>(response);
        }

        public async Task<InfoActivityTypeModel> Update(int id, UpdateActivityTypeModel updateActivityType)
        {
            var response = await PutAsyncInternal($"activitytype/{id}", updateActivityType);
            if (response == null || !response.IsSuccessStatusCode)
            {
                throw new HttpRequestException(await response.Content.ReadAsStringAsync());
            }
            return await ResponseToObject<InfoActivityTypeModel>(response);
        }
    }
}
