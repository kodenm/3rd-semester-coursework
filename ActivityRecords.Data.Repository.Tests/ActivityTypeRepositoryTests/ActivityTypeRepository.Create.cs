﻿using ActivityRecords.Data.Repository.Model.Entities;
using ActivityRecords.Data.Repository.Model.Exceptions;
using NUnit.Framework;
using System.Linq;

namespace ActivityRecords.Data.Repository.Tests
{
    public partial class ActivityTypeRepositoryTests
    {
        [Test]
        public void ShouldCreateActivityTypeWithFullData()
        {
            // arrange
            ActivityType inputActivityType = new ActivityType
            {
                Name = "TestActivity1",
                DetailNames = new[]
                {
                    "TestDetail1",
                    "TestDetail2"
                }
            };
            User user = new User
            {
                Login = "testlogin1"
            };
            int userId;

            // act
            userId = _userRepository.Create(user).Id;
            inputActivityType.UserId = userId;
            ActivityType resultActivityType = null;
            ActivityType queryActivityType = null;
            TestDelegate createActivityType = () =>
            {
                resultActivityType = _activityTypeRepository.Create(inputActivityType);
            };
            TestDelegate getActivityType = () =>
            {
                queryActivityType = _activityTypeRepository.GetById(resultActivityType.Id);
            };

            // assert
            Assert.DoesNotThrow(createActivityType);
            Assert.DoesNotThrow(getActivityType);
            Assert.IsTrue(ActivityTypesAreEqual(inputActivityType, resultActivityType));
            Assert.IsTrue(ActivityTypesAreEqual(resultActivityType, queryActivityType));
        }

        [Test]
        public void ShouldCreateActivityTypeWithoutDetailNames()
        {
            // arrange
            ActivityType inputActivityType = new ActivityType
            {
                Name = "TestActivity1",
                DetailNames = null
            };
            User user = new User
            {
                Login = "testlogin1"
            };
            int userId;

            // act
            userId = _userRepository.Create(user).Id;
            inputActivityType.UserId = userId;
            ActivityType resultActivityType = null;
            ActivityType queryActivityType = null;
            TestDelegate createActivityType = () =>
            {
                resultActivityType = _activityTypeRepository.Create(inputActivityType);
            };
            TestDelegate getActivityType = () =>
            {
                queryActivityType = _activityTypeRepository.GetById(resultActivityType.Id);
            };

            // assert
            Assert.DoesNotThrow(createActivityType);
            Assert.DoesNotThrow(getActivityType);
            Assert.IsTrue(ActivityTypesAreEqual(inputActivityType, resultActivityType));
            Assert.IsTrue(ActivityTypesAreEqual(resultActivityType, queryActivityType));
        }

        [Test]
        public void ShouldThrowValidationExceptionOnCreationWithNullData()
        {
            // arrange
            ActivityType inputActivityType = new ActivityType
            {
                Name = null,
                DetailNames = null
            };
            User user = new User
            {
                Login = "testlogin1"
            };
            int userId;

            // act
            userId = _userRepository.Create(user).Id;
            inputActivityType.UserId = userId;
            ActivityType resultActivityType = null;
            TestDelegate createActivityType = () =>
            {
                resultActivityType = _activityTypeRepository.Create(inputActivityType);
            };

            // assert
            Assert.Throws(typeof(ValidationException), createActivityType);
            Assert.AreEqual(_context.ActivityTypes.Count(), 0);
        }

        [Test]
        public void ShouldThrowRepositoryExceptionWithNoUserId()
        {
            // arrange
            ActivityType inputActivityType = new ActivityType
            {
                Name = "TestActivity1",
                DetailNames = null
            };

            // act
            ActivityType resultActivityType = null;
            TestDelegate createActivityType = () => resultActivityType = _activityTypeRepository.Create(inputActivityType);

            // assert
            Assert.Throws(typeof(RepositoryException), createActivityType);
            Assert.AreEqual(_context.ActivityTypes.Count(), 0);
        }
    }
}
