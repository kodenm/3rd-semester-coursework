﻿using ActivityRecords.Data.Repository.Model.Entities;
using ActivityRecords.Data.Repository.Model.Exceptions;
using NUnit.Framework;

namespace ActivityRecords.Data.Repository.Tests
{
    public partial class ActivityTypeRepositoryTests
    {
        [Test]
        public void ShouldUpdateActivityTypeWithFullData()
        {
            // arrange
            ActivityType inputActivityType = new ActivityType
            {
                Name = "TestActivity1",
                DetailNames = new[]
                {
                    "TestDetail1",
                    "TestDetail2"
                }
            };
            User user = new User
            {
                Login = "testlogin1"
            };
            int userId;
            ActivityType updateActivityType = new ActivityType
            {
                Name = "NewTestActivity1",
                DetailNames = new[]
                {
                    "NewTestDetail1",
                    "NewTestDetail2"
                }
            };

            // act
            userId = _userRepository.Create(user).Id;
            inputActivityType.UserId = userId;
            inputActivityType = _activityTypeRepository.Create(inputActivityType);
            ActivityType resultActivityType = null;
            TestDelegate changeActivityType = () => resultActivityType = _activityTypeRepository.Update(inputActivityType.Id, updateActivityType);
            ActivityType queryActivityType = null;
            TestDelegate getActivityType = () => queryActivityType = _activityTypeRepository.GetById(inputActivityType.Id);

            // assert
            Assert.DoesNotThrow(changeActivityType);
            Assert.DoesNotThrow(getActivityType);
            Assert.IsTrue(ActivityTypesAreEqual(inputActivityType, resultActivityType));
            Assert.IsTrue(ActivityTypesAreEqual(resultActivityType, queryActivityType));
        }

        [Test]
        public void ShouldUpdateActivityTypeWithOnlyName()
        {
            // arrange
            ActivityType inputActivityType = new ActivityType
            {
                Name = "TestActivity1",
                DetailNames = new[]
                {
                    "TestDetail1",
                    "TestDetail2"
                }
            };
            User user = new User
            {
                Login = "testlogin1"
            };
            int userId;
            ActivityType updateActivityType = new ActivityType
            {
                Name = "NewTestActivity1",
                DetailNames = null
            };

            // act
            userId = _userRepository.Create(user).Id;
            inputActivityType.UserId = userId;
            inputActivityType = _activityTypeRepository.Create(inputActivityType);
            ActivityType resultActivityType = null;
            TestDelegate changeActivityType = () => resultActivityType = _activityTypeRepository.Update(inputActivityType.Id, updateActivityType);
            ActivityType queryActivityType = null;
            TestDelegate getActivityType = () => queryActivityType = _activityTypeRepository.GetById(inputActivityType.Id);

            // assert
            Assert.DoesNotThrow(changeActivityType);
            Assert.DoesNotThrow(getActivityType);
            Assert.IsTrue(ActivityTypesAreEqual(inputActivityType, resultActivityType));
            Assert.IsTrue(ActivityTypesAreEqual(resultActivityType, queryActivityType));
        }

        [Test]
        public void ShouldThrowRepositoryExceptionIfUpdatesNonExistingActivityType()
        {
            // arrange
            int id = -1;
            ActivityType updateActivityType = new ActivityType
            {
                Name = "NewTestActivity1",
                DetailNames = null
            };

            // act
            ActivityType resultActivityType = null; 
            TestDelegate changeActivityType = () => resultActivityType = _activityTypeRepository.Update(id, updateActivityType);

            // assert
            Assert.Throws(typeof(RepositoryException), changeActivityType);
        }
    }
}
