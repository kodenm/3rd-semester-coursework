﻿using ActivityRecords.Data.Repository.MapperProfiles;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System.Configuration;
using System.Transactions;

namespace ActivityRecords.Data.Repository.Tests
{
    public class TestBase
    {
        protected DataContext _context;
        protected TransactionScope _transactionScope;
        protected IMapper _mapper;

        public void SetUp()
        {
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseSqlServer(ConfigurationManager.ConnectionStrings["TestDB"].ConnectionString)
                .Options;
            MapperConfiguration mc = new MapperConfiguration(opts =>
            {
                opts.AddProfile(typeof(UserProfile));
                opts.AddProfile(typeof(ActivityTypeProfile));
                opts.AddProfile(typeof(ActivityTypeDetailProfile));
                opts.AddProfile(typeof(ActivityProfile));
                opts.AddProfile(typeof(ActivityDetailProfile));
            });
            _mapper = mc.CreateMapper();
            _context = new DataContext(options);
            _context.Database.Migrate();
        }

        [OneTimeTearDown]
        public void TearDownDatabase()
        {
            _context.Database.EnsureDeleted();
        }

        [SetUp]
        public void TestSetUp()
        {
            _transactionScope = new TransactionScope();
        }

        [TearDown]
        public void TestTearDown()
        {
            _transactionScope.Dispose();
        }
    }
}
