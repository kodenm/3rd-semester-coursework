﻿using ActivityRecords.Data.Repository.Model.Entities;
using ActivityRecords.Data.Repository.Model.Exceptions;
using NUnit.Framework;
using System;

namespace ActivityRecords.Data.Repository.Tests
{
    public partial class UserRepositoryTests
    {
        [Test]
        public void ShouldUpdateUserWithFullData()
        {
            // arrange
            User creationUser = new User
            {
                Login = "testlogin1"
            };
            int id;
            User updateUser = new User
            {
                Login = "newtestlogin1",
                BirthDate = new DateTime(2000, 1, 1),
                Weight = 1,
                Height = 1
            };

            // act
            id = _repository.Create(creationUser).Id;
            User resultUser = null;
            User queryUser = null;
            TestDelegate changeUser = () =>
            {
                resultUser = _repository.Update(id, updateUser);
                queryUser = _repository.GetById(id);
            };

            // assert
            Assert.DoesNotThrow(changeUser);
            Assert.IsTrue(UsersAreEqual(updateUser, resultUser));
            Assert.IsTrue(UsersAreEqual(resultUser, queryUser));
        }

        [Test]
        public void ShouldUpdateUserWithNoLogin()
        {
            // arrange
            User creationUser = new User
            {
                Login = "testlogin1"
            };
            int id;
            User updateUser = new User
            {
                BirthDate = new DateTime(2000, 1, 1),
                Weight = 1,
                Height = 1
            };
            User expectedUser = new User
            {
                Login = "testlogin1",
                BirthDate = new DateTime(2000, 1, 1),
                Weight = 1,
                Height = 1
            };

            // act
            id = _repository.Create(creationUser).Id;
            expectedUser.Id = id;
            User updateResultUser = null;
            TestDelegate changeUser = () =>
            {
                updateResultUser = _repository.Update(id, updateUser);
                expectedUser.RegistrationDateTime = updateResultUser.RegistrationDateTime;
            };

            // assert
            Assert.DoesNotThrow(changeUser);
            Assert.IsNotNull(updateResultUser);
            Assert.IsTrue(UsersAreEqual(expectedUser, updateResultUser));
        }

        [Test]
        public void ShouldThrowRepositoryExceptionOnUpdateOfNonExistingUser()
        {
            // arrange
            int id = -1;

            User updateUser = new User
            {
                Login = "testlogin1"
            };

            // act
            User resultUser = null;
            TestDelegate changeUser = () =>
            {
                resultUser = _repository.Update(id, updateUser);
            };

            // assert
            Assert.Throws(typeof(RepositoryException), changeUser);
        }

        [Test]
        public void ShouldThrowRepositoryExceptionOnUpdateWithExistingLogin()
        {
            // arrange
            User otherUser = new User
            {
                Login = "testlogin1"
            };
            User creationUser = new User
            {
                Login = "testlogin2"
            };
            User updateUser = new User
            {
                Login = "testlogin1"
            };

            // act
            otherUser = _repository.Create(otherUser);
            creationUser = _repository.Create(creationUser);
            TestDelegate changeUser = () =>
            {
                updateUser = _repository.Update(creationUser.Id, updateUser);
            };

            // assert
            Assert.Throws(typeof(RepositoryException), changeUser);
        }
    }
}
