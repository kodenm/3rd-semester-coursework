﻿using ActivityRecords.Data.Repository.Model.Entities;
using ActivityRecords.Data.Repository.Model.Exceptions;
using NUnit.Framework;
using System;
using System.Linq;

namespace ActivityRecords.Data.Repository.Tests
{
    public partial class UserRepositoryTests
    {
        private bool UsersAreEqual(User user1, User user2)
        {
            return user1.Id == user2.Id
                && user1.Login == user2.Login
                && user1.BirthDate == user2.BirthDate
                && user1.Weight == user2.Weight
                && user1.Height == user2.Height;
        }

        [Test]
        public void ShouldCreateUserWithFullData()
        {
            // arrange
            User inputUser = new User
            {
                Id = 1,
                Login = "testlogin1",
                BirthDate = DateTime.Now,
                Weight = 1,
                Height = 2
            };

            // act
            User resultUser = null; 
            User queryUser = null;
            TestDelegate createUser = () =>
            {
                resultUser = _repository.Create(inputUser);
                queryUser = _repository.GetById(resultUser.Id);
            };

            // assert
            Assert.DoesNotThrow(createUser);
            Assert.IsTrue(UsersAreEqual(inputUser, resultUser));
            Assert.IsTrue(UsersAreEqual(resultUser, queryUser));
        }

        [Test]
        public void ShouldCreateUserWithMinimumRequiredData()
        {
            // arrange
            User inputUser = new User
            {
                Login = "testlogin1"
            };
            
            // act
            User resultUser = null;
            User queryUser = null;
            TestDelegate createUser = () =>
            {
                resultUser = _repository.Create(inputUser);
                queryUser = _repository.GetById(resultUser.Id);
            };

            // assert
            Assert.DoesNotThrow(createUser);
            Assert.IsTrue(UsersAreEqual(inputUser, resultUser));
            Assert.IsTrue(UsersAreEqual(resultUser, queryUser));
        }

        [Test]
        public void ShouldThrowRepositoryExceptionOnNewUserWithExistingLogin()
        {
            // arrange
            User existingUser = new User
            {
                Login = "existinglogin1"
            };
            User newUser = new User
            {
                Login = "existinglogin1"
            };

            // act
            _repository.Create(existingUser);
            TestDelegate createUser = () =>
            {
                newUser = _repository.Create(newUser);
            };

            // assert
            Assert.Throws(typeof(RepositoryException), createUser);
        }
    }
}
