﻿using ActivityRecords.Data.Repository.Repositories;
using NUnit.Framework;

namespace ActivityRecords.Data.Repository.Tests
{
    public partial class UserRepositoryTests : TestBase
    {
        private UserRepository _repository;

        [OneTimeSetUp]
        public new void SetUp()
        {
            base.SetUp();
            _repository = new UserRepository(_context, _mapper);
        }
    }
}
