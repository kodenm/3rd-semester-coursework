﻿using ActivityRecords.Data.Repository.Model.Entities;
using ActivityRecords.Data.Repository.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace ActivityRecords.Data.Repository.Tests
{
    [TestFixture]
    public partial class ActivityRepositoryTests : TestBase
    {
        private ActivityRepository _activityRepository;
        private ActivityTypeRepository _activityTypeRepository;
        private UserRepository _userRepository;

        [OneTimeSetUp]
        public new void SetUp()
        {
            base.SetUp();
            _activityTypeRepository = new ActivityTypeRepository(_context, _mapper);
            _userRepository = new UserRepository(_context, _mapper);
            _activityRepository = new ActivityRepository(_context, _mapper);
        }

        private bool ActivitiesAreEqual(Activity activity1, Activity activity2)
        {
            return activity1.ActivityTypeId == activity2.ActivityTypeId
                && activity1.UserId == activity2.UserId
                && Enumerable.SequenceEqual(activity1.Details, activity2.Details, new ActivityDetailCompaper());
        }

        private class ActivityDetailCompaper : IEqualityComparer<ActivityDetail>
        {
            public bool Equals([AllowNull] ActivityDetail x, [AllowNull] ActivityDetail y)
            {
                return x.Name == y.Name && x.Value == y.Value;
            }

            public int GetHashCode([DisallowNull] ActivityDetail obj)
            {
                throw new NotImplementedException();
            }
        }
    }
}
