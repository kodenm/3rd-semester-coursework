﻿using ActivityRecords.Data.Repository.Model.Entities;
using ActivityRecords.Data.Repository.Model.Exceptions;
using NUnit.Framework;
using System;

namespace ActivityRecords.Data.Repository.Tests
{
    public partial class ActivityRepositoryTests
    {
        [Test]
        public void ShouldReturnUserOnUpdateWithCorrectData()
        {
            // arrange
            User user = new User
            {
                Login = "testlogin1"
            };
            ActivityType activityType = new ActivityType
            {
                Name = "ActivityType1",
                DetailNames = new[]
                {
                    "TestDetail1",
                    "TestDetail2"
                }
            };
            Activity initialActivity = new Activity
            {
                Date = DateTime.Now,
                Details = new[]
                {
                    new ActivityDetail { Name = "TestDetail1", Value = "1" },
                    new ActivityDetail { Name = "TestDetail2", Value = "1" }
                }
            };

            // act
            user = _userRepository.Create(user);
            activityType.UserId = user.Id;
            activityType = _activityTypeRepository.Create(activityType);
            initialActivity.UserId = user.Id;
            initialActivity.ActivityTypeId = activityType.Id;
            Activity resultActivity = null;
            Activity queryActivity = null;
            TestDelegate changeActivity = () => resultActivity = _activityRepository.Create(initialActivity);
            TestDelegate getActivity = () => queryActivity = _activityRepository.GetById(resultActivity.Id);

            // assert
            Assert.DoesNotThrow(changeActivity);
            Assert.DoesNotThrow(getActivity);
            Assert.IsTrue(ActivitiesAreEqual(resultActivity, initialActivity));
            Assert.IsTrue(ActivitiesAreEqual(resultActivity, queryActivity));
        }

        [Test]
        public void ShouldThrowRepositoryExceptionOnUpdateOfNonExistingActivity()
        {
            // arrange
            int id = -1;
            Activity updatedActivity = new Activity
            {
                Details = new[]
                {
                    new ActivityDetail { Name = "TestDetail1", Value = "2" },
                    new ActivityDetail { Name = "TestDetail2", Value = "2" }
                }
            };

            // act
            TestDelegate changeActivity = () => _activityRepository.Update(id, updatedActivity);

            // assert
            Assert.That(changeActivity, Throws.TypeOf(typeof(RepositoryException)).And.Message.EqualTo("Activity doesn't exist"));
        }

        [Test]
        public void ShouldThrowValidationExceptionOnUpdateWithDifferentDetailNames()
        {
            // arrange
            User user = new User
            {
                Login = "testlogin1"
            };
            ActivityType activityType = new ActivityType
            {
                Name = "TestActivityType1",
                DetailNames = new[]
                {
                    "TestDetail1",
                    "TestDetail2"
                }
            };
            Activity initialActivity = new Activity
            {
                Details = new[]
                {
                    new ActivityDetail { Name = "TestDetail1", Value = "1" },
                    new ActivityDetail { Name = "TestDetail2", Value = "1" }
                }
            };
            Activity updatedActivity = new Activity
            {
                Details = new[]
                {
                    new ActivityDetail { Name = "TestDetail2", Value = "2" },
                    new ActivityDetail { Name = "TestDetail3", Value = "2" }
                }
            };

            // act
            initialActivity.UserId = activityType.UserId = _userRepository.Create(user).Id;
            initialActivity.ActivityTypeId = _activityTypeRepository.Create(activityType).Id;
            initialActivity = _activityRepository.Create(initialActivity);
            TestDelegate changeActivity = () => updatedActivity = _activityRepository.Update(initialActivity.Id, updatedActivity);

            // assert
            Assert.That(changeActivity,
                Throws.TypeOf(typeof(RepositoryException))
                .And.Message.EqualTo("List of details from updated activity doesn't match ones from activity type"));
        }
    }
}
