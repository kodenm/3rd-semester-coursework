import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { User } from '../interfaces/user';
import { JwtService } from './jwt.service';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  userId: number = -1;
  constructor(private jwtService: JwtService, private userService: UserService, private cookieService: CookieService, private router: Router) {
    if (jwtService.getToken()) {
      this.userId = Number(jwtService.getPayload().id);
    }
  }

  login(userForm: User): void {
    this.userService.login(userForm.login ?? "", userForm.password ?? "").subscribe((response: any) => {
      this.jwtService.setToken(response.accessToken);
      this.userId = Number(this.jwtService.getPayload().id);
      this.router.navigate(['activities']);
    },
    (error: HttpErrorResponse) => alert(error.error));
  }
  
  signup(userForm: User): void {
    let test = this.userService.signup(userForm).subscribe((user: any) => {
      this.router.navigate(['login']);
    },
    (error: HttpErrorResponse) => alert(error.error));
  }

  logout(): void {
    this.jwtService.removeToken();
    this.userId = -1;
    this.router.navigate(['login']);
  }
}
