import { Injectable } from '@angular/core';
// import { CookieService } from 'ngx-cookie-service';
import { CookieOptionsProvider, CookieService, COOKIE_OPTIONS } from 'ngx-cookie';

@Injectable({
  providedIn: 'root'
})
export class JwtService {
  private token: string = "";
  constructor(private cookieService: CookieService) {
    this.token = cookieService.get("accessToken");
  }

  setToken(token: string): void {
    if (token) {
      this.token = token;
      this.cookieService.put("accessToken", this.token.toString());
    }
  }

  getToken(): string {
    return this.token;
  }

  getPayload(): any {
    if (this.token) {
      let encodedPayload = this.token.split(".")[1];
      return JSON.parse(atob(encodedPayload));
    }
    return undefined;
  }

  removeToken(): void {
    this.cookieService.remove("accessToken");
  }
}
