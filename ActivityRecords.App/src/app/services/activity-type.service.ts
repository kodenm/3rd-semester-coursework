import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, EMPTY, empty } from 'rxjs';
import { environment } from '../environment';
import { Activity } from '../interfaces/activity';
import { ActivityType } from '../interfaces/activity-type';
import { AccountService } from './account.service';

@Injectable({
  providedIn: 'root'
})
export class ActivityTypeService {
  constructor(private http: HttpClient, private accountService: AccountService) { }

  getById(id: number) : Observable<ActivityType> {
    return this.http.get<ActivityType>(`${environment.gatewayServiceUrl}/activitytype/${id}`);
  }

  createNewActivityType(activityType: ActivityType): Observable<ActivityType> {
    return this.http.post<ActivityType>(`${environment.gatewayServiceUrl}/activitytype`, activityType);
  }
  
  getActivitiesByUserId(): Observable<ActivityType[]> {
    return this.http.get<ActivityType[]>(`${environment.gatewayServiceUrl}/activitytype/user/${this.accountService.userId}`);
  }

  removeById(id: number): Observable<any> {
    if (id > 0) {
      return this.http.delete(`${environment.gatewayServiceUrl}/activitytype/${id}`);
    }
    return EMPTY;
  }

  update(id: number, activityType: ActivityType): Observable<ActivityType> {
    return this.http.put<ActivityType>(`${environment.gatewayServiceUrl}/activitytype/${id}`, activityType);
  }

  create(activity: Activity): Observable<Activity> {
    return this.http.post<Activity>(`${environment.gatewayServiceUrl}/activity`, activity);
  }

}
