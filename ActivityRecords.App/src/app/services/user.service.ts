import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/app/environment';
import { User } from '../interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  user?: User;
  constructor(private http: HttpClient) { }

  getUser(id: number): Observable<User> {
    return this.http.get<User>(`${environment.gatewayServiceUrl}/user/${id}`);
  }

  signup(userForm: User): Observable<User> {
    return this.http.post(`${environment.gatewayServiceUrl}/user`, userForm);
  }

  updateUser(userForm: User): Observable<User> {
    return this.http.put(`${environment.gatewayServiceUrl}/user/${userForm.id}`, userForm);
  }

  login(login: string, password: string): Observable<User> {
    return this.http.post(`${environment.identityServiceUrl}/login`, { login: login, password: password });
  }
}
