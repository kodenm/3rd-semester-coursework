import { Pipe, PipeTransform } from '@angular/core';
import { Activity } from '../interfaces/activity';

@Pipe({
  name: 'sortByDate'
})
export class SortByDatePipe implements PipeTransform {
  transform(value: any, args: any): any {
    if (args !== 'asc' && args != 'desc') {
      args = 'asc';
    }
    let sign: number = args === 'asc' ? -1 : 1;
    const sorted = (value as Activity[])?.sort((a, b) => {
      return sign * (new Date(a.dateTime ?? 0).getTime() - new Date(b.dateTime ?? 0).getTime());
    });
    return sorted;
  }
}