import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button'
import { MatToolbarModule } from '@angular/material/toolbar'
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { CookieModule } from 'ngx-cookie';
import { ActivitiesComponent } from './components/pages/activities/activities.component';
import { ActivityTypesComponent } from './components/pages/activity-types/activity-types.component';
import { ProfileComponent } from './components/pages/profile/profile.component';
import { AuthFormComponent } from './components/pages/auth-form/auth-form.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ActivityTypeCreationComponent } from './components/elements/activity-type-creation/activity-type-creation.component';
import { ActivityTypeCardComponent } from './components/elements/activity-type-card/activity-type-card.component';
import { ActivityCardComponent } from './components/elements/activity-card/activity-card.component';
import { MatSelectModule } from '@angular/material/select';
import { ActivityCreationComponent } from './components/elements/activity-creation/activity-creation.component';
import { NgpSortModule } from "ngp-sort-pipe";
import { SortByDatePipe } from './pipes/sort-by-date.pipe';
import { JwtInterceptor } from './interceptors/jwt.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    ActivitiesComponent,
    ActivityTypesComponent,
    ProfileComponent,
    AuthFormComponent,
    ActivityTypeCreationComponent,
    ActivityTypeCardComponent,
    ActivityCardComponent,
    ActivityCreationComponent,
    SortByDatePipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MatCardModule,
    MatInputModule,
    MatToolbarModule,
    MatButtonModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatInputModule,
    ReactiveFormsModule,
    FormsModule,
    MatSelectModule,
    NgpSortModule,
    CookieModule.forRoot()
  ],
  providers: [
    CookieService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }  
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
