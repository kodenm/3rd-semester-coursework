import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ActivityCreationComponent } from './components/elements/activity-creation/activity-creation.component';
import { ActivitiesComponent } from './components/pages/activities/activities.component';
import { ActivityTypesComponent } from './components/pages/activity-types/activity-types.component';
import { AuthFormComponent } from './components/pages/auth-form/auth-form.component';
import { ProfileComponent } from './components/pages/profile/profile.component';
import { AuthGuard } from './services/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'activities', pathMatch: 'full' },
  { path: 'login', component: AuthFormComponent },
  { path: 'signup', component: AuthFormComponent },
  { path: 'activities', component: ActivitiesComponent, canActivate: [AuthGuard] },
  { path: 'activities/type/:typeId', component: ActivitiesComponent, canActivate: [AuthGuard] },
  { path: 'activities/create', component: ActivityCreationComponent, canActivate: [AuthGuard] },
  { path: 'activity-types', component: ActivityTypesComponent, canActivate: [AuthGuard] },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: '**', redirectTo: 'activities' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
