import { Component, Input, OnInit, Output } from '@angular/core';
import { Activity } from 'src/app/interfaces/activity';
import { ActivityType } from 'src/app/interfaces/activity-type';
import { ActivityTypeService } from 'src/app/services/activity-type.service';
import { EventEmitter } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { ActivityService } from 'src/app/services/activity.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-activity-card',
  templateUrl: './activity-card.component.html',
  styleUrls: ['./activity-card.component.css', '../../../shared.styles.css']
})
export class ActivityCardComponent implements OnInit {
  @Input() activity: Activity = {};
  @Output() onDeleted: EventEmitter<Activity> = new EventEmitter();
  activityType: ActivityType = {};
  detailsForm: FormGroup = new FormGroup({
    details: new FormArray([])
  });
  isDetailed: boolean = false;
  isEditMode: boolean = false;
  errorText: string = "";
  constructor(private activityTypeService: ActivityTypeService, private activityService: ActivityService) { }

  ngOnInit(): void {
    this.activityTypeService.getById(this.activity.activityTypeId ?? -1).subscribe(activityType => this.activityType = activityType);
  }

  toggleDetails(): void {
    this.isDetailed = !this.isDetailed;
  }

  toggleEditMode(): void {
    this.isEditMode = !this.isEditMode;
    if (this.isEditMode) {
      this.isDetailed = false;
      let formArray = this.getDetailsFormArray();
      this.getDetailsFormArray().controls.map(c => c as FormGroup);
      formArray.clear();
      this.activity.details?.forEach(d => formArray.push(new FormGroup({
        name: new FormControl(d.name),
        value: new FormControl(d.value)
      })));
    }
  }

  remove(): void {
    this.onDeleted.emit(this.activity);
  }

  onSubmit(): void {
    this.nullEmptyFields(this.getDetailFormGroups());
    this.activityService.update(this.activity.id ?? -1, this.detailsForm.getRawValue() as Activity).subscribe(
      (a) => {
        this.activity = a;
        this.isEditMode = false;
      },
      (error: HttpErrorResponse) => this.errorText = error.error
    );
  }

  getDetailsFormArray(): FormArray {
    return this.detailsForm.controls['details'] as FormArray;
  }

  getDetailFormGroups(): FormGroup[] {
    return this.getDetailsFormArray().controls.map(c => c as FormGroup);
  }

  nullEmptyFields(formGroups: FormGroup[]): void {
    formGroups.forEach(g => {
      let valueControl: FormControl = g.controls['value'] as FormControl;
      if (!valueControl.value) {
        valueControl.setValue(null);
      }
    });
  }
}
