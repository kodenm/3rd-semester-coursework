import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityTypeCreationComponent } from './activity-type-creation.component';

describe('ActivityTypeCreationComponent', () => {
  let component: ActivityTypeCreationComponent;
  let fixture: ComponentFixture<ActivityTypeCreationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActivityTypeCreationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityTypeCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
