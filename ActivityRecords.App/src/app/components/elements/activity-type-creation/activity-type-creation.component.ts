import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { FormControl, FormGroup, FormArray, Validators } from '@angular/forms';
import { CustomValidators } from 'src/app/custom-validators';
import { ActivityType } from 'src/app/interfaces/activity-type';
import { AccountService } from 'src/app/services/account.service';
import { ActivityTypeService } from 'src/app/services/activity-type.service';

@Component({
  selector: 'app-activity-type-creation',
  templateUrl: './activity-type-creation.component.html',
  styleUrls: ['./activity-type-creation.component.css', '../../../shared.styles.css']
})
export class ActivityTypeCreationComponent implements OnInit {
  errorText: string = "";
  @Output() onCreated = new EventEmitter();
  @Output() onCanceled = new EventEmitter();
  formGroup: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required, CustomValidators.lettersAndDigitsWithSpace()]),
    detailNames: new FormArray([], CustomValidators.formArrayIsUnique())
  });
  constructor(private accountService: AccountService, private activityTypeService: ActivityTypeService) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
    let activityType: ActivityType = this.formGroup.value as ActivityType;
    activityType.userId = this.accountService.userId;
    this.activityTypeService.createNewActivityType(activityType).subscribe((resultActivityType: ActivityType) => { 
      this.onCreated.emit(resultActivityType);
    }, (error: HttpErrorResponse) => {
      this.errorText = error.error;
    });
  }

  Cancel(): void {
    this.onCanceled.emit();
  }

  addNewDetail(): void {
    this.getDetails().push(new FormControl('', [Validators.required, CustomValidators.lettersAndDigitsWithSpace()]));
  }

  getDetails(): FormArray {
    return this.formGroup.controls['detailNames'] as FormArray;
  }

  removeDetail(index: number): void {
    this.getDetails().removeAt(index);
  }
}
