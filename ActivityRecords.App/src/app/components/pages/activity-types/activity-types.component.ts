import { Component, OnInit } from '@angular/core';
import { ActivityType } from 'src/app/interfaces/activity-type';
import { AccountService } from 'src/app/services/account.service';
import { ActivityTypeService } from 'src/app/services/activity-type.service';

@Component({
  selector: 'app-activity-types',
  templateUrl: './activity-types.component.html',
  styleUrls: ['./activity-types.component.css', '../../../shared.styles.css']
})
export class ActivityTypesComponent implements OnInit {
  activityTypes: ActivityType[] = [];
  newActivityTypeCreated: boolean = false;
  constructor(private activityTypeService: ActivityTypeService, private accountService: AccountService) { }

  ngOnInit(): void {
    this.activityTypeService
      .getActivitiesByUserId()
      .subscribe(activityTypes => this.activityTypes = activityTypes);
  }

  toggleCreateForm(element: any): void {
    this.newActivityTypeCreated = !this.newActivityTypeCreated;
  }

  remove(activityType: ActivityType): void {
    this.activityTypeService.removeById(activityType.id ?? -1).subscribe(() => { 
      let index: number = this.activityTypes.indexOf(activityType);
      this.activityTypes.splice(index, 1);
    });
  }

  onActivityTypeCreated(activityType: ActivityType): void {
    this.activityTypes.push(activityType);
    this.newActivityTypeCreated = false;
  }

  onActivityTypeDeleted(activityType: ActivityType): void {
    this.activityTypeService.removeById(activityType.id ?? -1).subscribe(() => { 
      let index: number = this.activityTypes.indexOf(activityType);
      this.activityTypes.splice(index, 1);
    });
  }
}
