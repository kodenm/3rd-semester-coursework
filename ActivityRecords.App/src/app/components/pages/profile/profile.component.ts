import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { CustomValidators } from 'src/app/custom-validators';
import { User } from 'src/app/interfaces/user';
import { AccountService } from 'src/app/services/account.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css', '../../../shared.styles.css']
})
export class ProfileComponent implements OnInit {
  user?: User;
  formGroup: FormGroup = new FormGroup({
    id: new FormControl(''),
    login: new FormControl('', [Validators.required, CustomValidators.lettersAndDigits()]),
    birthDate: new FormControl(''),
    weight: new FormControl('', CustomValidators.isPositiveNumber(true)),
    height: new FormControl('', CustomValidators.isPositiveNumber(true))
  })
  isProfileEditMode: boolean = false;
  isPasswordEditMode: boolean = false;
  errorText: string = "";
  constructor(private accountService: AccountService, private userService: UserService) { }

  ngOnInit(): void {
    this.userService.getUser(this.accountService.userId).subscribe((user: any) => {
      this.user = user;
      this.formGroup.patchValue({
        id: user.id,
        login: user.login,
        birthDate:  user.birthDate ?  new Date(user.birthDate).toISOString().substr(0, 10) : undefined,
        weight: user.weight,
        height: user.height
      });
    });
  }

  enableProfileEditMode(): void {
    this.isProfileEditMode = !this.isProfileEditMode;
    this.formGroup.removeControl('password');
  }

  enablePasswordEditMode(): void {
    this.isPasswordEditMode = !this.isPasswordEditMode;
    this.formGroup.addControl('password', new FormControl('', [Validators.required, CustomValidators.lettersAndDigits()]));
  }

  resetModes(): void {
    this.isPasswordEditMode = false;
    this.isProfileEditMode = false;
    this.formGroup.reset();
    let user = (this.user as any);
    this.formGroup.patchValue({
      id: this.user?.id,
      login: this.user?.login,
      birthDate: user.birthDate ? new Date(user.birthDate).toISOString().substr(0, 10) : undefined,
      weight: this.user?.weight,
      height: this.user?.height
    });
    this.formGroup.removeControl('password');
  }

  onSubmit(): void {
    Object.keys(this.formGroup.controls).forEach(key => {
      this.formGroup.controls[key].setValue(this.formGroup.controls[key].value ? this.formGroup.controls[key].value : undefined);
    });
    let user: User = this.formGroup.value as User;
    this.formGroup.controls['login'].setValue(this.formGroup.controls['login'].value.toLowerCase());
    this.userService.updateUser(user).subscribe(() => {
      this.userService.getUser(user.id ?? 0).subscribe((user: any) => {
        this.user = user;
        this.formGroup.patchValue({
          id: user.id,
          login: user.login,
          birthDate: new Date(user.birthDate).toISOString().substr(0, 10),
          weight: user.weight,
          height: user.height
        });
      });
    },
    (error: HttpErrorResponse) => {
      alert(error.error);
    },
    () => {
      this.resetModes();
    });
  }
}
