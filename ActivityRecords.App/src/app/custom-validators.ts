import { AbstractControl, FormArray, ValidationErrors, ValidatorFn, Validators } from "@angular/forms";

export class CustomValidators {
    static lettersAndDigits(): ValidatorFn { return Validators.pattern("^[a-zA-Z0-9]+$"); }
    static lettersAndDigitsWithSpace(): ValidatorFn { return Validators.pattern("^[a-zA-Z0-9 ]+$"); } 
    static formArrayIsUnique(): ValidatorFn {
        return (control: AbstractControl): ValidationErrors | null => {
            let formArray: FormArray = control as FormArray;
            let set: Set<string> = new Set(formArray.controls.map(c => c.value));
            return set.size === formArray.controls.length ? null : { "notUnique": true }
        };
    }
    static isPositiveNumber(nullable: boolean): ValidatorFn {
        return (control: AbstractControl): ValidationErrors | null => {
            return (nullable ? control.value === null : false) || control.value > 0 ? null : { "nan": true };
        };
    }
}