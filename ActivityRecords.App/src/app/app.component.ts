import { Component } from '@angular/core';
import { ActivationEnd, ActivationStart, NavigationEnd, Router } from '@angular/router';
import { AccountService } from './services/account.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  /**
   *
   */
  navigationEnd: string = "";
  constructor(public accountService: AccountService, private router: Router) {
    router.events.forEach(e => {
      if (e instanceof NavigationEnd) {
        this.navigationEnd = e.url;
      }
    })
  }
  title = 'ActivityRecords';

  onLogout(): void {
    this.accountService.logout();
    this.router.navigate(['login']);
  }
}
