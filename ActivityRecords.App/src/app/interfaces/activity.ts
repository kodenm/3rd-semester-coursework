import { LocationChangeEvent } from "@angular/common";
import { ActivityDetail } from "./activity-detail";

export interface Activity {
    id?: number;
    activityTypeId?: number;
    userId?: number;
    dateTime?: Date;
    details?: ActivityDetail[];
}
