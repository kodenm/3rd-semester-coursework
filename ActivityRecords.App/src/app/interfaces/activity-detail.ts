export interface ActivityDetail {
    name: string;
    value: string;
}
