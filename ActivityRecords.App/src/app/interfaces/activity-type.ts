export interface ActivityType {
    id?: number;
    name?: string;
    userId?: number;
    detailNames?: string[];
}