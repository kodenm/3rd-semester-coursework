export interface User {
    id?: number;
    login?: string;
    password?: string;
    registrationDateTime?: Date;
    birthDate?: Date;
    weight?: number;
    height?: number;
}
